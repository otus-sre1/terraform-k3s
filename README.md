# Terraform k3s

```shell
terraform init && \
  terraform plan -var-file=variables.tfvars && \
  terraform apply -var-file=variables.tfvars -auto-approve
```

```shell
kubectl get namespaces                                           
NAME              STATUS   AGE
default           Active   15m
kube-system       Active   15m
kube-public       Active   15m
kube-node-lease   Active   15m
application       Active   18s
```

```shell
kubectl describe namespaces application
Name:         application
Labels:       kubernetes.io/metadata.name=application
Annotations:  <none>
Status:       Active

No resource quota.

Resource Limits
 Type                   Resource  Min  Max   Default Request  Default Limit  Max Limit/Request Ratio
 ----                   --------  ---  ---   ---------------  -------------  -----------------------
 Pod                    cpu       -    200m  -                -              -
 Pod                    memory    -    1Gi   -                -              -
 PersistentVolumeClaim  storage   24M  -     -                -              -
 Container              cpu       -    -     50m              50m            -
 Container              memory    -    -     24Mi             24Mi           -
```

```shell
kubectl describe roles.rbac.authorization.k8s.io --namespace application
Name:         role-example
Labels:       test=MyRole
Annotations:  <none>
PolicyRule:
  Resources         Non-Resource URLs  Resource Names  Verbs
  ---------         -----------------  --------------  -----
  deployments.apps  []                 []              [list get]
  pods              []                 [foo]           [list watch get]
```

```shell
kubectl describe rolebindings.rbac.authorization.k8s.io --namespace application
Name:         role-binding
Labels:       <none>
Annotations:  <none>
Role:
  Kind:  Role
  Name:  role-example
Subjects:
  Kind            Name            Namespace
  ----            ----            ---------
  User            role-example    default
  ServiceAccount  default         application
  Group           system:masters  default
```

```shell
terraform destroy -var-file=variables.tfvars -auto-approve
```
