variable "name" {
  description = "Name of the namespace to be created"
  type        = string
  validation {
    condition     = var.name != ""
    error_message = "The name of the namespace cannot be null or empty."
  }
}

variable "kubectl_config_context_name" {
  description = "The config context to use when authenticating to the Kubernetes cluster."
  type        = string
  validation {
    condition     = var.kubectl_config_context_name != ""
    error_message = "The name of the context cannot be null or empty."
  }
}

variable "kubectl_config_path" {
  description = "The path to the config file to use for kubectl."
  type        = string
  validation {
    condition     = var.kubectl_config_path != ""
    error_message = "The path to the config file cannot be missing."
  }
}

variable "limit_pod_max_cpu" {
  description = "CPU limit for pod"
  type        = string
}

variable "limit_pod_max_memory" {
  description = "Memory limit for pod"
  type        = string
}

variable "limit_pvc_min_storage" {
  description = "Minimum size for PVC"
  type        = string
}

variable "limit_container_default_cpu" {
  description = "CPU default value for container"
  type        = string
}

variable "limit_container_default_memory" {
  description = "Memory default value for container"
  type        = string
}


variable "namespace_admins" {
  type = object({
    users  = list(string)
    groups = list(string)
  })
  default = {
    users  = []
    groups = []
  }
  description = "Defines the users and groups that will be admins in the namespace."
}
