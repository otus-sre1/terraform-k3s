resource "kubernetes_limit_range" "limit-range-namespece" {
  metadata {
    name = "limit-range-namespece"
    namespace = var.name
  }
  spec {
    limit {
      type = "Pod"
      max = {
        cpu    = var.limit_pod_max_cpu
        memory = var.limit_pod_max_memory
      }
    }
    limit {
      type = "PersistentVolumeClaim"
      min = {
        storage = var.limit_pvc_min_storage
      }
    }
    limit {
      type = "Container"
      default = {
        cpu    = var.limit_container_default_cpu
        memory = var.limit_container_default_memory
      }
    }
  }
}
